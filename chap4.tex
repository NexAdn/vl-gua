\chapter{Färbungen}
\section{Greedy-Färbung}

\begin{definition}{Anticlique}
	Eine \defname{} ist eine Menge paarweise nicht benachbarter Ecken.
\end{definition}

\begin{definition}{Färbung}\label{def:farbung1}
	Eine \(k\)-\defname{} von \(G\) ist eine Partition von \(V\left(G\right)\)
	in höchstens \(k\) Anticliquen.
\end{definition}

Alternativ lassen sich \(k\)-Färbungen auch als Abbildungen betrachtet.
Daraus ergibt sich die klassische Definition für Färbungen:

\begin{definition}{Färbung}\label{def:farbung2}
	Eine \(k\)-\defname{} von \(G\) ist eine Abbildung \(f: V\left(G\right) \to C\)
	in eine \(k\)-elementige Menge \(C\)
	mit \(f\left(x\right) \neq f\left(y\right)\) für \(xy \in E\left(G\right)\).
\end{definition}

Eine Färbung in Form von Anticliquen lässt sich in eine Färbung
in Form einer Funktion umwandeln,
indem jeder Anticlique eine Zahl zugeordnet wird
und die Ecken in den Anticliquen als Farbe die der Anticlique zugeordnete Zahl erhalten.
Umgekehrt können Anticliquen gebildet werden,
indem Ecken mit der gleichen Farbei jeweils zu einer Anticlique zusammengefasst werden.
Über Färbbarkeit lässt sich also immer über beide Möglichkeiten
(finden einer Funktion \(f\) entsprechend \autoref{def:farbung2}
oder Bildung von \(k\) Anticliquen entsprechend \autoref{def:farbung1}) argumentieren.

\begin{definition}{chromatische Zahl}
	Für \(G\) sei die \defname{} \(\chromnum\left(G\right)\) definiert als
	\begin{align}
		\min \left\{k \geq 0 \mmid G \text{ ist } k\text{-färbbar}\right\}\text{.}
	\end{align}
\end{definition}
Einige Zusammenhänge zwischen Eigenschaften des Graphen
und der chromatischen Zahl sind bereits bekannt:
\begin{align}
	\chromnum\left(G\right) = 0 &\iff G = \left(\emptyset, \emptyset\right)\\
	\chromnum\left(G\right) \leq 1 &\iff E\left(G\right) = \emptyset\\
	\chromnum\left(G\right) \leq 2 &\iff G \text{ hat keine Kreise ungerader Länge}\\
\end{align}

Das Entscheidungsproblem \(\chromnum\left(G\right) \leq 3\) ist bereits NP-vollständig.\footnote{
	Das Problem lässt sich auf 3-SAT, ein bekanntes NP-vollständiges Problem,
	reduzieren.
}
Deshalb ist es nützlich,
wenn zumindest Schranken für die Färbungszahl gewonnen werden können.
Eine mögliche Abschätzung ist im Folgenden gezeigt.

Definiere zunächst
\begin{align}
	\omega\left(G\right) &= \max\left\{\cabs{A} \mmid A \text{ Clique in } G\right\}
\end{align}
Dabei sind \cdefname{Cliquen}\index{Clique} Mengen von benachbarten Ecken.
Es gilt \(\chromnum\left(G\right) \geq \omega\left(G\right)\).
Dies gilt intuitiv,
da eine Clique mit \(k\) paarweise benachbarten Ecken auch mindestens \(k\) Farben benötigt,
damit keine zwei benachbarten Ecken die gleiche Farbe haben.
Allerdings ist auch die Bestimmung von \(\omega\left(G\right)\) NP-vollständig.

Als Gegenstück zu \(\omega\left(G\right)\) kann man auch \(\alpha\) definieren als:
\begin{align}
	\alpha\left(G\right) &= \max\left\{\cabs{A} \mmid A \text{ Anticlique in } G\right\}
\end{align}
\(G\) hat \(\chromnum\left(G\right)\)-Färbung \(\mathcal{C}\)
(Partition in \(\chromnum\left(G\right)\) Anticliquen).
Daraus folgt
\begin{align}
	\cabs{V\left(G\right)} &\leq \chromnum\left(G\right) \cdot \alpha\left(G\right)
	\implies \chromnum\left(G\right) &\geq \frac{\cabs{V\left(G\right)}}{\alpha\left(G\right)}
\end{align}
für \(G \neq \left(\emptyset, \emptyset\right)\).

Die Anticliquenzahl ist gleich der Cliquenzahl des Komplementärgraphen.
Dieser lässt sich bilden,
indem die Ecken miteinander verbunden werden,
die im ursprünglichen Graphen nicht miteinander verbunden sind.
Entsprechend ist auch die Bestimmung der Anticliquenzahl ein NP-vollständiges Problem.

Beispielsweise findet man im Graph in \autoref{img:5er-clique} eine 5er-Clique.
Also benötigt man mindestens \(5\) Farben für die Graphenfärbung.
Es zeigt sich auch durch Beispiel, dass der Graph \(5\)-färbbar ist.
Da \(\omega\left(G\right) = 5\) eine untere Schranke ist,
ist also \(\chromnum\left(G\right) = 5\).

\input{img/5er-clique}

Der \textsc{Petersen}-Graph ist sogar \(3\)-färbbar \(\chromnum\left(G\right) \geq 3\),
da sich relativ leicht eine \(3\)-Färbung findet,
wie in \autoref{img:petersen-3farb} gezeigt.
Da es Kreise ungerader Länge im Graphen gibt,
muss entsprechend dem Satz von \textsc{Tuza} (\autoref{satz:tuza})
auch \(\chromnum\left(G\right) \geq 3\) gelten.
Also gilt \(\chromnum\left(G\right) = 3\).
Hier zeigt sich auch, dass die Cliquenzahl nur eine untere Schranke ist,
da \(\omega\left(G\right) = 2\) für den \textsc{Petersen}-Graph ist,
jedoch \(\chromnum\left(G\right) = 3\).

\input{img/petersen-faerbung}

\begin{definition}{planar}\index{plattbar@plättbar}
	\(G\) ist \defname{}/\cdefname{plättbar},
	falls eine „überschneidungsfreie“ Zeichnung des Graphen in der Ebene möglich ist.
\end{definition}

Ein Beispiel für einen planaren Graphen ist das 7-Rad,
gezeigt in \autoref{img:7-rad-faerbung}.
Für diese gilt der sogenannte \emph{Vierfarbensatz}.

\input{img/7-rad-faerbung.tex}

\begin{satz}[Vier-Farben-Satz]
	\(\chromnum\left(G\right) = 4\) für planare Graphen \(G\).
\end{satz}

Zum Beweis dieses Satzes ist nach wie vor Rechenaufwand mit Computern notwendig.
Bis heute ist kein Beweis für diesen Satz vorhanden,
er wird jedoch trotzdem als bewiesen angesehen.

Anwendung findet der Satz auch auf Landkarten.
Durch Abbildung der Länder durch ihre Hauptstädte und Verbindung der Hauptstädte
untereinander, wenn sie auf der Landkarte benachbart sind,
erzeugt einen solchen planaren Graphen, der dual zur Landkarte ist.
Da dieser Überschneidungsfrei ist,
kann man eine Landkarte also mit vier Farben immer so einfärben,
dass keine zwei benachbarten Länder die gleiche Farbe bekommen.
Sonderfälle wie Exklaven und Kolonien, die den Graphen nicht mehr planar machen,
werden dabei nicht berücksichtigt!

\begin{definition}[Grad!Ecke]{Grad}
	Der \defname{} \(d_G\left(x\right)\) einer Ecke \(x\) im Graphen \(G\)
	ist die Anzahl der mit \(x\) inzidenten Kanten,
	also in diesem Modell (ohne Mehrfachkanten) die Anzahl der Nachbarn von \(x\).

	\begin{align}
		\delta\left(G\right) &:= \min\left\{d_G\left(x\right) \mmid x \in V\left(G\right)\right\}\\
		\Delta\left(G\right) &:= \max\left\{d_G\left(x\right) \mmid x \in V\left(G\right)\right\}
	\end{align}
	sind der \cdefname{Minimalgrad}\index{Grad!Minimal-} bzw.
	\cdefname{Maximalgrad}\index{Grad!Maximal-}.
\end{definition}

Beachte, dass bei der Benutzung von Mehrfachkanten der Grad nicht mehr gleich der Anzahl der Nachbarn ist,
da es möglich ist, dass zwei oder mehr Kanten zwischen \(x\) und einem Nachbarn liegen.

Sei \(k > 0\) eine Zahl und \(x_1, \dotsc, x_n\) eine Aufzählung der Ecken von \(G\)
mit
\begin{align}
	\cabs{N_G\left(x_{l+1}\right) \cap \left\{x_1, \dotsc, x_n\right\}} < k \quad\forall l \in \left\{1, \dotsc, n-1\right\}
\end{align}
(bspw. \(k = \Delta\left(G\right) + 1\) und eine beliebige Aufzählung).
Dann besitzt \(G\) eine \(k\)-Färbung.
Diese lässt sich induktiv konstruieren.

\begin{verfahren}\label{verf:greedy-farb}
	Seien \(x_1, \dotsc, x_l\) bereits gefärbt
	(d.\,h.~\(G\left[\left\{x_1, \dotsc, x_l\right\}\right]\) besitzt eine Färbung \(f\),
	etwa mit den Farben \(1, \dotsc, k\).
	Weil \(x_{l+1}\) weniger als \(k\) Nachbar in \(\left\{x_1, \dotsc, x_l\right\}\) hat,
	gibt es eine Farbe \(c \in \left\{1, \dotsc, k\right\}\) mit
	\(f\left(x_j\right) \neq c\) für alle Nachbarn \(x_j\) von \(x_{l+1}\)
	in \(\left\{x_1, \dotsc, x_{l}\right\}\).
	Durch \(f\left(x_{l+1}\right) = c\) erhält man eine Färbung \(f\) von
	\(G\left[\left\{x_1, \dotsc, x_l, x_{l+1}\right\}\right]\).
	So entsteht schließlich eine Färbung von ganz \(G\).
\end{verfahren}

Diese \(k\)-Färbung funktioniert ähnlich,
wie es für den \textsc{Petersen}-Graph gemacht wurde.
Man färbt der Reihe nach die Ecken \(x_1, \dotsc, x_l\)
mit einer beliebigen freien Farbe.

\input{img/graph-aufzaehlung-k}

Damit ist es möglich, mit einem möglichst kleinen \(k\)
die Färbungszahl nach oben hin zu beschränken.

\begin{definition}{Reihenzahl}
	Das kleinste \(k\),
	für das es eine Aufzählung \(x_1, \dotsc, x_n\) der Ecken von \(G\) wie oben gibt,
	heißt \defname{} von \(G\),
	bezeichnet als \(\colnum\left(G\right)\).
\end{definition}

\begin{satz}
	Für jeden Graphen \(G\) ist
	\(\chromnum\left(G\right) \leq \colnum\left(G\right) \leq \Delta\left(G\right) + 1\).
	Für vollständige Graphen \(K_n\) und Kreise \(C_l\) ungerader Länge \(l\)
	ist \(\chromnum\left(G\right) = \Delta\left(G\right) + 1\),
	d.\,h. hier gilt Gleichheit.
\end{satz}

Bis auf einige Sonderfälle ist \(\chromnum\left(G\right)\) kleiner als \(\Delta\left(G\right)\).
Für die Reihenzahl ist hier noch keine einfache Bestimmungsmethode eingeführt worden.
Zunächst betrachten wir eine alternative Darstellung der Reihenzahl:

\begin{satz}
	Für jeden Graphen \(G\) ist
	\begin{align}
		\colnum\left(G\right) = \max\left\{\delta\left(H\right)
			\mmid H \text{ induzierter Teilgraph von } G\right\}
	\end{align}
	Die Teilgraphen \(H\) sind dabei von der Form
	\(H = G\left[A\right]\) für \(A \subseteq V\left(G\right)\).
\end{satz}

\begin{beweis}
	Sei \(D := \max\left\{\delta\left(H\right) \mmid H \text{ Teilgraph von } G\right\}\).
	Definiere induktiv (abwärts) eine Aufzählung von \(V\left(G\right)\).
	Sind \(x_{l+1}, x_{l+2}, \dotsc, x_n\) schon definiert,
	so sei \(x_l\) eine Ecke minimalen Grades in
	\(H = G - \left\{x_{l+1}, \dotsc, x_n\right\} = G\left[V\left(G\right) \setminus \left\{x_{l+1}, \dotsc, x_n\right\}\right]\).

	Danach gilt
	\begin{align}
		\cabs{N_G\left(x_l\right) \cap \left\{x_1, \dotsc, x_l\right\}} = \delta\left(H\right)
		< \leq D < D+1
	\end{align}
	Also ist \(\colnum\left(G\right) \leq D + 1\).

	Sei umgekehrt \(x_1, \dotsc, x_n\) eine Aufzählung von \(V\left(G\right)\),
	die die Reihenzahl \(k = \colnum\left(G\right)\) realisiert.
	Sei \(H\) ein induzierter Teilgraph von \(G\) und
	\(x_{i_1}, x_{i_2}, \dotsc, x_{i_h}\) die induzierte Aufzählung seiner Ecken
	(\(i_1 < i_2 < \dotsc < i_n\).
	Dann gilt
	\begin{align}
		\delta\left(H\right) &\leq \cabs{N_H\left(x_{i_H}\right)}\\
		&= \cabs{N_G\left(x_{i_h}\right) \cap \left\{x_{i_1}, x_{i_2}, \dotsc, x_{i_{h-1}}\right\}}\\
		&\leq \cabs{N_G\left(x_{i_h}\right) \cap \left\{x_{1}, x_{2}, \dotsc, x_{\left(i_h - 1\right)}\right\}}\\
		&< k\text{.}
	\end{align}
	Dann gilt \(k > \delta\left(H\right)\), also
	\begin{align}
		k &> \max\left\{\delta\left(H\right) \mmid H \text{ induzierter Teilgraph von } G\right\}\\
		&= D\\
		\implies \colnum\left(G\right) &= D + 1
	\end{align}
\end{beweis}

\section{Die Sätze von Brooks und Vizing}

Betrachte \autoref{img:5-rad-kempe}.
Wenn man für kleinere Graphen bereits eine Färbung gefunden hat,
ist man interessiert,
eine Färbung für eine Erweiterung des Graphen zu finden.
Dafür muss man möglicherweise eine bestehende Färbung modifizieren,
um die Färbung zu organisieren, sodass eine günstigere Färbung für die Erweiterung entsteht.

\input{img/5-rad-kempe.tex}

Dafür wähle man zunächst zwei Farben aus und beobachte alle Ecken,
die durch diese Farben gefärbt sind.
Man kann nun versuchen, in dem durch diese Ecken und
die diese Ecken verbindenden Kanten induzierten Teilgraphen die Farben zu tauschen.
Dies ist offensichtlich möglich.
Für Ecken, die aus dem induzierten Teilgraphen hinausführen,
ist dies auch möglich.
Dadurch entsteht eine neue Färbung des Graphen.
Dies ist auch für nur einzelne Komponenten, dieses Teilgraphen möglich,
sodass bspw. Ecken ohne Nachbarn im induzierten Graphen nicht umgefärbt werden müssen.
Ein solcher Tausch der Färbung wird auch als \cdefname{Kempe-Tausch}\index{Kempe-Tausch}
bezeichnet.

\begin{definition}{Kempe-Kette}
	Ist \(\mathcal{C}\) eine \(k\)-Färbung des Graphen \(G\)
	(Partition in höchstens \(k\) Anticliquen)
	und sind \(A \neq B\) verschiedene Farbklassen (Anticliquen) aus C.
	So heißt eine Komponente \(H\) des Graphen \(G\left[A \cup B\right]\),
	eine \(A,B\)-\defname{}.
\end{definition}

\begin{definition}{Kempe-Tausch}
	Die Menge \(A \symdiff V\left(H\right)\) ist eine Anticlique in \(G\).
	Dies gilt, da \(A \setminus H\left(G\right)\) offensichtlich eine Anticlique ist
	und \(V\left(H\right) \setminus A = V\left(H\right) \cap B\) auch eine Anticlique ist
	(da ja \(B\) bereits eine Anticlique ist).
	Also ist
	\begin{align}
		\mathcal{C}\left(H\right) &:= \left(\mathcal{C} \setminus \left\{A, B\right\}\right)
			\cup \left\{A \symdiff V\left(H\right), B \symdiff V\left(H\right)\right\}
	\end{align}
	ebenfalls eine \(k\)-Färbung von \(G\).
	\(\mathcal{C}\left(H\right)\) entsteht aus \(\mathcal{C}\) durch \defname{}
	entlant der \(A,B\)-Kempe-Kette \(H\).
\end{definition}

Sätze wie der Vier-Farben-Satz wurden häufig versucht zu beweisen.
Oftmals wurden diese mithilfe von mehrfachen Kempe-Täuschen durchgeführt.
Meist sind dabei Rechenfehler aufgetreten,
die dann die Ursache waren, weshalb der Beweis fehlerhaft ist.

Generell lassen sich jedoch mithilfe von Kempe-Täuschen Bedingungen für
den Fünf-Farben-Satz (und Sätze mit mehr Farben) zeigen,
die für die Grade der Ecken in planaren Graphen gelten müssen,
damit die Graphen mit der jeweiligen Anzahl Farben färbbar ist.

\begin{satz}[\textsc{Brooks}]
	Sei \(G\) ein zusammenhängender Graph.
	Dann gilt \(\chromnum\left(G\right) \leq \Delta\left(G\right) + 1\) und
	dort Gleichheit genau dann,
	wenn \(G\) ein vollständiger Graph ist oder ein Kreis ungerader Länge.
\end{satz}

\begin{beweis}
	Die Behauptung ist richtig für vollständige Graphen und für \(\Delta\left(G\right) \leq 2\).
	Sei \(G\) ein Gegenbeispiel mit möglichst wenigen Kanten
	und \(\Delta := \Delta\left(G\right)\).
	Dann hat \(G\) eine Kante \(xy\) und \(\Delta \geq 3\).
	Außerdem besitzt \(G - xy\) eine \(\Delta\)-Färbung \(\mathcal{C}\)
	(diskutiere den Fall \(G-xy\) unzusammenhängend ist
	bzw. \(G - xy\) ein zusammenhängender Ausnahmegraph ist).
	nach Wahl von \(G\).
	Dann liegen \(x\) und \(y\) in der selben Farbklasse \(A\) aus \(\mathcal{C}\).
	Sei \(B \neq A\) aus \(\mathcal{C}\).
	Dann liegen \(x, y\) in derselben \(A,B\)-Kempe-Kette
	(sonst führt ein Kempe-Tausch bei der \(A,B\)-Kempe-Kette, die \(x\) enthält,
	auf eine Färbung, in der \(x,y\) unterschiedlichen Farbklassen angehören).
	Jede der \(\Delta-1\) vielen Farben \(\neq A\) kommt bei
	wenigstens einem Nachbarn von \(x\) in \(G - xy\) vor.
	(Träfe das für \(C \neq A\) nicht zu, könnte man \(x\) mit \(C\) statt \(A\) färben.)
	Also kommt jede der \(\Delta - 1\) vielen Farben \(\neq A\)
	bei genau einem Nachbarn von \(x\) in \(G - xy\) vor.
	\(x\) (und \(y\)) haben also in der \(A,B\)-Kempe-Kette, die \(x\) und \(y\) enthält,
	jeweils den Grad \(1\).

	Gäbe es eine innere Ecke des \(x,y\)-Weges in der \(A,B\)-Kempe-Kette,
	die \(x\) und \(y\) enthält, mit mehr als zwei Nachbarn in dieser Kempe-Kette,
	so wähle eine solche mit möglichst kleinem Abstand auf dem Weg zu \(x\), etwa \(z\).
	Also hat \(z\) drei Nachbarn aus \(A\) (oder aus \(B\)).
	Also tritt wenigstens eine der \(\Delta - 2\) von \(A, B\) verschiedenen Farben
	in der Nachbarschaft von \(z\) nicht auf.
	Die Umfärbung von \(z\) mit Farbe \(C\) liefert eine Färbung,
	bei der \(x, y\) nicht in derselben \(A, B\)-Kempe-Kette liegen.\contrad

	Darum ist die \(A, B\)-Kempe-Kette, die \(x\) und \(y\) enthält,
	ein Weg von \(x\) nach \(y\).
	Dies ist der Fall für \emph{jede} von \(A\) verschiedene Farbe \(B\), etwa für \(C\).
	
	Haben, die \(A,B\)-Kempe-Kette und die \(A,C\)-Kempe-Kette mindestens
	eine gemeinsame Ecke \(z\),
	so treten die Farben \(B\) und \(C\) jeweils genau zweimal
	unter den Nachbarn von \(z\) in \(G - xy\) auf.
	Daraus folgt, dass eine der \(\Delta - 3\) von \(A, B, C\) verschiedenen Farben,
	etwa \(D\), dort nicht auftritt.
	Wähle \(z\) möglichst nahe bei \(x\) in der \(A, B\)-Kempe-Kette.\footnote{
		Dies ist zwar nicht zwangsläufig notwendig,
		gibt aber zusätzliche Kontrolle darüber,
		wie die das Anfangsstück der \(A,B\)-Kempe-Kette auszusehen hat.
	}
	Die Umfärbung von \(z\) mit \(D\) führt auf eine Färbung,
	in der \(x, y\) nicht in derselben \(A, B\)-Kempe-Kette liegen.

	Also bilden die \(A,B\)-Kempe-Ketten, \(B \neq A\),
	die \(x\) und \(y\) enthalten,
	ein System von \(\Delta - 1\) vielen offen disjunkten \(x,y\)-Wegen
	(für jede Färbung \(\mathcal{C}\), für jede Kante \(xy\) von \(G\)).

	\subparagraph*{Falsche Fortführung}
	Wir wollen zeigen: die \(A, B\)-Kempe-Kette \(P\),
	die \(x, y\) enthält,
	hat keine inneren Ecken aus \(A\) (d.\,h. \(P\) hat Länge 2).
	Wäre \(z\) eine solche, so betrachte die \(A, C\)-Kempe-Kette \(H\),
	die \(z\) enthält; diese enthält nicht \(x, y\).
	Durch Kempe-Tausch entlang \(H\) entsteht eine Färbung,
	\emph{in der \(x, y\) in verschiedenen \(A,B\)-Kempe-Ketten liegen.}

	Achtung! Es ist nicht grundsätzlich der Fall,
	dass danach \(x, y\) in verschiedenen Kempe-Ketten liegen.
	Stattdessen ist es möglich, dass durch die Umfärbung
	die Kette irgendwo anders wieder verbunden wird!

	\subparagraph*{(Hoffentlich) richtige Fortführung}
	Sei \(y'\) der Nachbar von \(x\) auf der \(A,B\)-Kempe-Kette,
	die \(x, y\) enthält.
	Betrachte den Graphen \(\left(G - xy\right) - xy'\)
	(Unterbrechung ebenjener \(A,B\)-Kempe-Kette direkt hinter \(x\))
	und färbe dort die \(A,B\)-Kempe-Kette, die \(y\) enthält, um.
	Es entsteht eine Färbung \(\mathcal{C}'\) von \(G - xy'\),
	in der \(x, y'\) mit \(A\) gefärbt sind.
	Diese hat dieselben Eigenschaften wie (sinngemäß) \(\mathcal{C}\).

	Die \(A, B\)-Kempe-Kette, die \(x,y\) enthält in \(\mathcal{C}'\) und \(G - xy'\)
	entsteht aus der \(A,B\)-Kempe-Kette \(P'\), die \(x, y\) enthält in \(\mathcal{C}\),
	die \(x, y\) enthält, \(G - xy\), wie folgt:
	\begin{align}
		P' &= \left(P - x\right) + x
	\end{align}
	Die \(A,C\)-Kempe-Kette in \(\mathcal{C}'\), \(G - xy'\), \(Q\) entsteht
	darum aus der \(A,C\)-Kempe-Kette in \(\mathcal{C}\) und \(G - xy\)
	durch \(Q' = \left(Q - y\right) + y'\).

	Sei \(x'\) der von \(x\) verbliebene Nachbar von \(y'\) auf der \(A,B\)-Kempe-Kette,
	die \(x, y\) enthält (in \(\mathcal{C}, G - xy\)).
	Durch Farbtausch bei \(x\) und \(y'\) entsteht eine Färbung \(\mathcal{C}''\)
	von \(G - y'x'\).
	Die \(A,B\)-Kempe-Kette dort, die \(x', y'\) enthält,
	ist \(\left(P - \left\{x, y'\right\}\right) + x + y'\)
	die \(A,C\)-Kempe-Kette dort, die \(x', y'\) enthält,
	hat eine Ecke des Grades \(\geq 3\)
	(nämlich den \(C\)-gefärbten Nachbarn von \(y'\),
	es sei denn, dieser ist zu \(x\) benachbart).
	Wie oben der Nachbar \(z'\) auf der \(A,C\)-Kette von \(G - xy\),
	\(\mathcal{C}\), die \(x, y\) enthält, ist zu \(x'\) benachbart.

	Aber: \(z'\) hat jetzt drei mit \(A\) gefärbte Nachbarn,
	nämlich \(x\), einen weiteren auf der \(A,C\)-Kempe-Kette und \(x'\).
	Darum bilden die \(A,B\)-Kempe-Ketten,
	\(B \neq A\) mit \(x, y\),
	ein System offen disjunkter Wege der Länge 2.
	Also gibt es für jede Kante \(xy\)
	\(\Delta - 1\) viele offendisjunkte Wege in \(G - xy\) der Länge 2.
	Also haben \(x, y\) dieselben \(\Delta - 1\) vielen Nachbarn in \(G - xy\).\footnote{
		Das bedeutet nicht automatisch, dass sie auch dieselben Nachbarn in \(G\) haben.
	}

	Ist \(z\) ein Nachbar von \(x \neq y\),
	so haben \(z, y\) dieselben \(\Delta - 1\) vielen Nachbarn in \(G - yz\).
	Also sind je zwei Nachbarn von \(x\) benachbart in \(G\).
	Also ist \(G\) ein vollständiger Graph auf \(\Delta + 1\) vielen Ecken.
\end{beweis}

Es sei anzumerken, dass es sich in der Vergangenheit gezeigt hat,
dass die Zuhilfenahme von Kempe-Ketten und Kempe-Tauschen,
leicht zu Fehlern im Beweis führen kann!

Alternativ zur bisher behandelten Knotenfärbung
kann man auch Kanten färben\todo{bild 4.5}:

\begin{definition}[Farbung@Färbung!Kante]{Kantenfärbung}
	Eine Funktion \(f: E\left(G\right) \to \left\{1, \dotsc, k\right\}\)
	heißt \(k\)-\defname{},
	falls \(f\left(e\right) \neq f\left(e'\right)\) für \(e \neq e'\)
	mit gemeinsamen Endecken.
\end{definition}

\begin{definition}[farbbar@färbbar!kanten-]{kantenfärbbar}
	\(G\) heißt \(k\)-\defname{},
	falls eine \(k\)-Kantenfärbung von \(G\) existiert.
	Die kleinste Zahl \(k\) so,
	dass \(G\) k-kantenfärbbar ist,
	heißt \cdefname{chromatischer Index}\index{chromatischer Index} von \(G\),
	Bezeichnung \(\chromidx\left(G\right)\).
\end{definition}

Im nächsten Satz wird gezeigt, dass der chromatische Index
durch den Maximalgrad \(\Delta\left(G\right)\)
sowohl nach oben als auch nach unten beschränkt wird.
Die Streuung des chromatischen Indexes wird also vollständig
durch den Maximalgrad beschrieben.

\begin{satz}[\textsc{Vizing}]\label{satz:vizing}
	Für jeden endlichen Graphen \(G\) gilt:
	\begin{align}
		\Delta\left(G\right) \leq \chromidx\left(G\right) \leq \Delta\left(G\right) + 1
	\end{align}
\end{satz}

Für Eckenfärbungen wurde bereits die Färbung als Partition der Eckenmenge
in Anticliquen aufgefasst.
Analog können Kantenfärbungen als Partition in Matchings aufgefasst werden.
Weiter lassen sich auch Kanten-Kempe-Ketten bilden.
Dennoch stellen sich Kantenfärbungsprobleme als ähnlich schwierig heraus.

\begin{definition}{fehlt}
	Ist \(f: E\left(G\right) \to \left\{1, \dotsc, k\right\}\) eine \(k\)-Kantenfärbung
	des Graphen \(G\) und \(i \in \left\{1, \dotsc, k\right\}, x \in V\left(G\right)\),
	so sagt man \(i\) \defname{} bei \(x\),
	falls \(i \notin f\left(E_G\left(x\right)\right)\),
	d.\,h.~keine mit \(x\) inzidente Kante hat Farbe \(i\).
\end{definition}

\begin{beweis}[\autoref{satz:vizing}]
	Sei \(G\) Graph vom Maximalgrad \(\Delta\left(G\right) \leq k\).
	Sei \(x \in V\left(G\right)\), \(F \subseteq E_G\left(x\right)\).

	(\(\ast\)) Gäbe es eine \(k\)-Kantenfärbung
	\(f: E\left(G - F\right) \to \left\{1, \dotsc, k\right\}\),
	sodass für jedes \(e \in F\) wenigstens eine Farbe bei beiden Endecken von \(e\) fehlt,
	mit einer Ausnahme sogar wenigstens zwei,
	dann existiert eine \(k\)-Färbung von \(G\).

	Beweise (\(\ast\)) induktiv über \(\cabs{F}\).
	Trivial für \(\cabs{F} = 0\) (und \(\cabs{F} = 1\)).\checkm

	Nach Voraussetzung gibt es \(g = xy \in F\)
	und für jede Kante \(e \in F\) eine Menge
	\(\overline{e} \subseteq \left\{1, \dotsc, k\right\}\) mit:
	\begin{enumerate}[(i)]
		\item Jedes \(i \in \overline{e}\) fehlt bei beiden Endecken von \(e\).
		\item \(\forall e \in F, \left\{g\right\}: \cabs{\overline{g}} = 1, \cabs{\overline{e}} = 2\)
	\end{enumerate}
	Falls es ein \(i \in \left\{1, \dotsc, k\right\}\) gibt
	in \emph{genau} einem \(\overline{e}\), \(e \in F\), vorkommt,
	so ist \(h := f \cup \left\{\left(e, i\right)\right\}\)
	(Fortsetzung von \(f\) durch \(h\left(e\right) := i\))
	eine \(k\)-Färbung von \(G - \left(F \setminus \left\{e\right\}\right)\)
	und die Voraussetzungen von (\(\ast\)) sind erfüllt für \(h, F\setminus \left\{e\right\}\)
	statt \(f, F\).
	Die Behauptung folgt dann induktiv.\checkm

	Andernfalls gibt es kein \(i \in \left\{1, \dotsc, k\right\}\),
	das in genau einem \(\overline{e}, e \in F\) vorkommt.
	Wegen \(k \geq d_G\left(x\right)\) gibt es wenigstens \(\cabs{F}\) Farben
	aus \(\left\{1, \dotsc, k\right\}\), die bei \(x\) fehlen;
	kommt jede von diesen in wenigstens einem \(\overline{e}\) vor,
	dann sogar in zwei verschiedenen \(\overline{e}\).
	\begin{align}
		\implies \sum_{e\in F} \cabs{e} &= 2\cabs{F}\contrad
	\end{align}

	\todo{bild 4.6 hier irgendwo}

	Daher gibt es eine Farbe \(i\), die bei \(x\) fehlt,
	aber in keinem \(\overline{e}\) vorkommt.
	Sei \(j\) die Farbe von \(\overline{g}\), \(i \neq j\).
	Somit fehlen \(i, j\) bei \(x\) Und \(j\) (vielleicht auch \(i\)) fehlt bei \(y\).
	Die Komponente \(P\) von \(G\left[f^{-1}\left(\left\{i, j\right\}\right)\right]\),
	die \(y\) enthält, ist ein \(y,z\)-Weg mit \(z \neq x\).\footnote{
		Dieser Wege könnte auch „kurz“ sein, \(P = y\), falls auch \(i\) bei \(y\) fehlt.
	}

	Durch „Farbtausch“ entlang \(P\) entsteht eine neue \(k\)-Kantenfärbung \(f'\)
	von \(G - F\), d.\,h.
	\begin{align}
		f'\left(e\right) &= \begin{cases}
			i &\text{falls } e \in E\left(P\right) \land f\left(e\right) = j\\
			j &\text{falls } e \in E\left(P\right) \land f\left(e\right) = i\\
			f\left(e\right) &\text{sonst}
		\end{cases}
	\end{align}

	\todo{bild 4.7 hier irgendwo}

	Die bei den Ecken aus \(G\) fehlenden Farben haben sich
	beim Übergang von \(f\) nach \(f'\) nur bei \(y\) oder \(z\) verändert.
	\(f'' := f' \cup \left\{\left(g, i\right)\right\}\) ist eine \(k\)-Färbung
	von \(G - \left(F \setminus \left\{g\right\}\right)\):
	Die bei den Ecken aus \(G\) fehlenden Farben haben sich beim Übergang
	von \(f\) und \(f''\) nur bei \(y\), \(z\) oder \(x\) verändert.
	Zu jedem \(e \in F \setminus \left\{g\right\}\)
	gibt es immer noch wenigstens eine Farbe, die bei den Endecken von \(e\) fehlt und
	für jedes \(e \in F\left\{g\right\}\) sogar zwei solche Farben mit
	höchstens einer Ausnahmekante \(g'\) (die dann \(xz\) ist).
	Daraus folgt induktiv (\(\ast\)).

	Jetzt die Behauptung des Satzes durch Induktion über \(\cabs{V\left(G\right)}\):
	Die Behauptung gilt offensichtlich für \(\cabs{V\left(G\right)} = 0\).
	Sei \(k = \Delta\left(G\right) + 1\) und \(x \in V\left(G\right)\).
	Dann besitzt \(G - x\) nach Induktionsvoraussetzung eine \(k\)-Kantenfärbung.
	Der Grad von \(y\) bei den Nachbarn \(y\) von \(x\) ist höchstens \(k-2\).
	Also fehlen bei jedem Nachbarn von \(x\) zwei Farben.
	Verwende nun (\(\ast\)) für \(F := E_G\left(x\right)\).
	Also ist \(G\) \(k\)-färbbar.
\end{beweis}

Graphen \(G\) mit \(\chromidx\left(G\right) = \Delta\left(G\right)\)
heißen \cdefname{Typ-1-Graphen}\index{Typ1@Typ-1};
Graphen mit \(\chromidx\left(G\right) = \Delta\left(G\right) + 1\)
wiederum \cdefname{Typ-2-Graphen}\index{Typ2@Typ-2}.
Für viele Probleme sind die Antworten für Typ-1-Graphen leichter zu finden
als für Typ-2-Graphen.
Es bietet sich also bei der Einschätzung der Schwierigkeit von Problemen an,
ob sich diese wenigstens für Typ-1-Graphen leicht lösen lassen.
Wenn nicht, ist es meist auch schwierig, das Problem für Typ-2-Graphen zu lösen.
Umgangssprachlich werden 2-reguläre Typ-2-Graphenauch als \emph{Snarks}
(nach den Wesen aus „Alice im Wunderland“) bezeichnet.
