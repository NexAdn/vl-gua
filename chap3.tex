\chapter{Flüsse}

\section{Das Max-Flow-Min-Cut-Theorem}

Optimierung in Flussnetzwerken hat zum Ziel,
einen Strom von irgendwas (bspw. Datenströme, Wasser) so durch das Netzwerk zu transportieren,
dass möglichst viel gleichzeitig durch das Netz von einer Quelle zu einer Senke transportiert werden kann.
Für diese Betrachtung werden jetzt gerichtete Graphen betrachtet.

\begin{definition}{Digraph}
	Ein \defname{} \(D = \left(V, E\right)\) besteht aus einer Menge \(V\)
	von \cdefname{Ecken}\index{Ecke}\index{Knoten}
	und einer Menge
	\(E \subseteq \left\{\left(x, y\right) \,\middle|\, x \neq y \in V\right\} = V^2 \ id_V\)
	von \cdefname{Kanten}\index{Kante} (engl. \emph{arc}\index{arc} statt \emph{edge}).
	Schreibe \(xy\) statt \(\left(x, y\right)\).
\end{definition}

Bei Digraphen ist es insbesondere auch möglich, dass es zwei Kanten zwischen zwei Ecken gibt,
nämlich wenn eine Kante von der ersten zur zweiten und
die andere Kante von der zweiten zur ersten Ecke geht.
Allerdings werden solche Fälle für Flussnetzwerke zunächst nicht betrachtet.

\todo{Bild 3.1}

Auch, wenn hier die Notation \(xy\) als Kurzschreibweise für die Kante \(\left(x, y\right)\)
genau wie für ungerichtete Graphen benutzt wird,
werden hier gerichtete Kanten bezeichnet.
Das bedeutet also, dass \(\left(x, y\right) = xy \neq yx = \left(y, x\right)\).

\begin{definition}{Außenkante}
	Für \(X, Y \subseteq V\) sei
	\begin{align}
		E_D\left(X, Y\right) &:= E \cap \left(X \times Y\right)
		= \left\{xy \in E \,\middle|\, x \in X, y \in Y\right\}\\
		\shortintertext{
			die Menge aller Kanten von \(X\) nach \(Y\).
			Für \(X \subseteq V\) ist
		}
		E_D^+\left(X\right) &:= E_D\left(X, V \setminus X\right)\\
		\shortintertext{
			die Menge der \cdefname{Außenkanten}\index{Außenkante} von \(X\)
			und
		}
		E_D^-\left(X\right) &:= E_D\left(V \setminus X, X\right)\\
		\shortintertext{
			die Menge der \cdefname{Innenkanten}\index{Innenkante} von \(X\).
			Für \(x \in V\):
		}
		E_D^+\left(x\right) &:= E_D^+\left(\left\{x\right\}\right)\\
		E_D^-\left(x\right) &:= E_D^-\left(\left\{x\right\}\right)\\
		\shortintertext{
			Der \cdefname{Außengrad}\index{Außengrad} einer Ecke \(x\) ist:
		}
		d_D^+\left(x\right) &:= \cabs{E_D^+\left(x\right)}\text{.}\\
		d_D^-\left(x\right) &:= \cabs{E_D^-\left(x\right)}
	\end{align}
	ist der \cdefname{Innengrad}\index{Innengrad} der Ecke \(x\).
\end{definition}

\todo{bild 3.2, bild 3.3}

\begin{definition}{zugrundeliegender ungerichteter Graph}
	Zu einem Digraphen ist der \cdefname{zugrundeliegende ungerichtete Graph}:
	\begin{align}
		U\left(D\right) &= \left(
			V\left(D\right),
			\left\{\left\{x, y\right\} \,\middle|\, \left(x, y\right) \in E\left(D\right)\right\}
		\right)
	\end{align}
\end{definition}

Die Definitionen für ungerichtete Graphen übertragen sich
(teilweise auch wortwörtlich) auf Digraphen bzw.
können auf dem einem Digraphen \(D\) zugrundeliegenden ungerichteten Graphen.
Man beachte jedoch,
dass beim zugrundeliegenden ungerichteten Graphen zu einem Digraphen
Informationen über die Richtungen der Kanten verloren gehen!

\begin{definition}[Kreis!kontinuierlich gerichtet]{kontinuierlich gerichtete Kreise}
	Aus Kreisen \(C = x_1, x_1, \dotsc, x_{l-1}, x_0\) werden
	\defname{}
	(d.\,h.~
	\(\forall i \in \left\{1, \dotsc, l-1\right\}: \left(x_{i-1}, x_i\right) \in E\left(D\right),
	\left(x_{l-1}, x_0\right) \in E\left(D\right)\)),
	aus Wegen \(P = x_0, \dotsc, x_l\) \cdefname{kontinuierlich gerichtete Wege}
	(d.\,h.~\(\forall i \in \left\{1, \dotsc, l\right\}: \left(x_{i-1}, x_i\right) \in E\left(D\right)\)).
\end{definition}

\todo{bild 3.4}

Die Relation \(x \sim_D y :\iff \exists x,y\)-Weg in \(D\) ist reflexiv und transitiv,
aber \emph{nicht} symmetrisch.
Die Existenz eines \(x,y\)-Weges impliziert nicht automatisch
die Existenz eines \(y,x\)-Weges!

\begin{definition}{}
	Für \(f: E\left(D\right) \to \mathbb{R}\) und \(F \subseteq E\left(D\right)\)
	sei
	\begin{align}
		f\left(F\right) &:= \sum_{e \in F} f\left(e\right)
	\end{align}
	(statt wie üblich \(\left\{f\left(e\right) \,\middle|\, e \in F\right\}\)).
\end{definition}

Während anderswo der Begriff \emph{Netzwerk} auch für Graphen allgemein benutzt wird,
gibt es in der Flusstheorie eine feste Definition für Netzwerke:

\begin{definition}{Netzwerk}
	Ein \defname{} ist ein Quadrupel \(N = \left(D, c, s, t\right)\)
	bestehend aus einem Digraphen \(D\),
	einer \cdefname{Kapazitätsfunkion}\index{Kapazität}
	\(c : E\left(D\right) \to \mathbb{R}_{\geq 0}\),
	einer \cdefname{Quelle}\index{Quelle} \(s \in V\left(D\right)\) und
	einer \cdefname{Senke}\index{Senke} \(t \in V\left(D\right) \setminus \left\{s\right\}\).
\end{definition}

\todo{bild 3.5 (ohne das blaue)}

Neben der Kapazitätsfunktion wird in Bildern üblicherweise auch noch die Flussfunktion eingezeichnet.
Diese gibt an, wie viel \emph{Fluss} über eine Kante geleitet wird.

\begin{definition}{Fluss}
	Ein \defname{} von \(N\) (auch: \(s,t\)-\defname{})
	ist eine Funktion \(f : E\left(D\right) \to \mathbb{R}_{\geq 0}\) mit:
	\begin{enumerate}[(i)]
		\item \(f\left(e\right) \leq c\left(e\right) \forall e \in E\left(D\right)\)
			(Der Fluss darf nie größer als die Kapazität einer Kante sein.)
		\item \(f\left(E_D^+\left(x\right)\right) = f\left(E_D^-\left(x\right)\right) \forall x \in V\left(D\right) \left\{s, t\right\}\)
			(Der eingehende Fluss einer Ecke muss genau so groß wie der ausgehende Fluss sein,
			außer für Quellen und Senken. Dies ist die sog. \textsc{Kirchoff}-Regel\index{Kirchhoff}.)
	\end{enumerate}
\end{definition}

Es darf also niemals eine Kante über ihre Kapzität hinaus ausgelastet werden
und Kanten können keinen Fluss „speichern“,
d.\,h.~sie müssen immer sämtlichen eingehenden Fluss wieder über ausgehende Kanten herausgeben.
Die einzige Ausnahme hierfür sind die Quelle und Senke.
Für alle anderen Ecken gilt
\begin{align}
	\sum_{e \in E_D^+\left(x\right)} f\left(e\right) &= \sum_{e \in E_D^-\left(x\right)} f\left(x\right)
\end{align}
Es ist prinzipiell auch möglich,
dass es Fluss in die Quelle hinein bzw. Fluss aus der Senke heraus gibt!

Eine interessante Optimierungsfrage ist jetzt,
wie viel Fluss maximal von \(s\) nach \(t\) „transportiert“ werden kann.

\begin{definition}{Stärke}
	Die \defname{} eines Flusses \(f\) im Netzwerk \(N\) ist:
	\begin{align}
		\flowstrength{f} &:= f\left(E_D^+\left(s\right)\right) - f\left(E_D^-\left(s\right)\right)
	\end{align}
\end{definition}

Als Beispiel sie hier beispielhafter Fluss in
\todo{bild 3.6}
gezeigt.
An jeder Ecke des markierten Weges kommt eine Einheit Fluss an
und eine Einheit Fluss geht aus (außer natürlich für Quelle und Senke).
Damit ist die \textsc{Kirchhoff}-Bedingung erfüllt.
Weiterhin ist der Fluss an jeder Kante kleiner als die Kapazität.
Da an der Quelle eine Einheit Fluss abgeht und keine eingeht,
ist die Stärke des Flusses \(\flowstrength{f} = 1\).
Man beachte in diesem Beispiel, dass der Weg über die oberen Ecken gar nicht genutzt wurde.
Man könnte also prinzipiell mehr Fluss durch das Netzwerk leiten als hier gezeigt,
indem Fluss über die oberen Kanten geleitet wird!

\begin{definition}{Elementarfluss}
	Ist \(P\) ein \(s,t\)-Weg in \(D\) und \(c > 0\),
	so wird durch
	\begin{align}
		f_P\left(e\right) = \begin{cases}
			c & e \in E\left(P\right)\\
			0 & \text{sonst}
		\end{cases}
	\end{align}
	ein Fluss \(f_P\) der Stärke \(c\) definiert,
	falls \(c \left(e\right) \geq c \forall e \in E\left(P\right)\) gilt.
	\(f_P\) heißt dann \defname{} von \(N\).
\end{definition}

Insbesondere ist \(f\) konstant \(0\) in jedem Netzwerk ein Fluss der Stärke 0.
Dieser ist jedoch in der Regel uninteressant.
Stattdessen stellt sich die Frage,
wie groß denn so ein Fluss höchstens werden kann.
Hierfür werden \emph{Schnitte} zur Hilfe gezogen,
die den Graphen in zwei Teile zerlegen,
wobei ein Teil \(s\) und ein Teil \(t\) enthält.
Dann kann man bestimmen, wie viel Fluss über die Kanten fließt,
die über die Schnittgrenze gehen.

\begin{definition}{Schnitt}
	Ein \defname{} in \(N\) ist eine Teilmenge \(X\) von \(V\) mit
	\(s \in X\) und \(t \notin X\).
	Die \cdefname{Kapazität}\index{Kapazität!Schnitt} \(\cutcap{X}\)
	ist die Zahl \(c\left(E_D^+\left(X\right)\right)\).
\end{definition}

\todo{bild 3.7}

Die Stärke eines Flusses lässt sich an den Zwischenkanten
eines beliebigen Schnittes \(x\) von \(N\) ablesen.
\begin{align}
	\flowstrength{f} &= \sum_{x\in X} f\left(E_D^+\left(x\right)\right) -
	\sum_{x \in X} f\left(E_D^-\left(x\right)\right)\\
	\intertext{
		Dies gilt aufgrund der \textsc{Kirchhoff}-Bedingung.
		Wenn man sich für \(x \neq s\) diese Summe anschaut,
		tritt im vorderen Teil immer die Summe des eingehenden Flusses auf,
		während hinten die Summe des ausgehenden Flusses auf.
		Für Flusse innerhalb von \(X\) heben sich diese Terme auf,
		also verbleibt ausschließlich die Stärke des Flusses.
		Man kann dies auch noch feingranularer betrachten:
	}
	&= \sum_{x \in X} \sum_{\substack{y \in V\\xy \in E}} f\left(xy\right)
	- \sum_{x \in X}\sum_{\substack{x \in V\\yx \in E}} f\left(yx\right)\\
	\shortintertext{
		Sobald man nur die Kanten betrachtet, die beide Endpunkte in \(X\) haben,
		sieht man sofort, dass der Fluss für diese immer einmal in der vorderen
		und einmal in der hinteren Summe auftritt.
		Am Ende verbleibt nur der Fluss über Kanten,
		die mit einer Ecke \(x \in X\) beginnen und in einer Ecke \(y \in V \setminus X\) enden:
	}
	&= \sum_{x \in X}\sum_{\substack{y \in V \setminus X\\xy \in E}} f\left(xy\right)
	- \sum_{x \in X}\sum_{\substack{y \in V \setminus X\\yx \in E}} f\left(yx\right)\\
	&= f\left(E_D^+\left(X\right)\right) - f\left(E_D^-\left(X\right)\right)
\end{align}
Man kann die Stärke eines Flusses nicht nur an bestimmten Schnitten,
sondern an jedem beliebigen Schnitt ablesen --
insbesondere von dem einfachen Fluss \(X = \left\{s\right\}\).
Das bedeutet aber auch, dass die Kapazitäten der Schnitte obere Schranken
für Flüsse sind!

\todo{bild 3.8}

\begin{align}
	\flowstrength{f} &= f\left(E_D^+\left(X\right)\right) -
		\underbrace{f\left(E_D^-\left(X\right)\right)}_{\geq 0}\\
	&\leq f\left(E_D^+\left(X\right)\right)\\
	&\overset{(i)}{\leq} c\left(E_D^+\left(X\right)\right)\\
	\implies \max\left\{\flowstrength{f} \,\middle|\, f \text{ ist Fluss von } N\right\}
	&\leq \min\left\{\cutcap{X} \,\middle|\, X \text{ ist Schnitt von } N\right\}
\end{align}

\begin{satz}[Max-Flow-Min-Cut-Theorem, \textsc{Ford-Fulkerson}]
	\index{MaxFlowMinCutTheorem@Max-Flow-Min-Cut-Theorem}
	\label{satz:maxflow-mincut}

	In jedem Netzwerk \(N\) gilt:
	\begin{align}
		\max\left\{\flowstrength{f} \,\middle|\, f \text{ Fluss von } N\right\}
		&= \min\left\{ \cutcap{X} \,\middle|\, X \text{ Schnitt von } N\right\}
	\end{align}
\end{satz}

\todo{bild 3.9}

Die Idee des Folgenden Beweises ist,
einen bestehenden Fluss schrittweise zu erhöhen.
Wir wissen bereits, dass es am Anfang immer irgendeinen Fluss
(bspw. einen Nullfluss) geben kann,
von dem aus gestartet wird.
Dazu werden zunächst sog. \emph{Residualwege} von \(s\) nach \(z\) definiert.
Dieser Weg hat eine Kapazität, die \emph{Residualkapazität},
welche angibt,
um wie viel der Fluss von \(s\) nach \(z\) auf dem Weg noch erhöht werden kann.

\begin{definition}{Residualweg}
	Sei \(N = \left(D, c, s, t\right)\) ein Netzwerk
	und \(f\) irgendein Fluss, \(z \in V\left(D\right)\).

	Ein \defname{} von \(s\) nach \(z\) ist
	eine Folge \(P = x_0, e_1, x_1, e_2, x_2, \dotsc, x_l\)
	aus paarweise verschiedenen Ecken \(x_0, \dotsc, x_l\)
	mit \(x_0 = s, x_l = z\)
	und \(e_i = x_{i-1}x_i\) (\cdefname{Vorwärtskante})
	oder \(e_i = x_ix_{i-1}\) (\cdefname{Rückwärtskante}).
	\index{Vorwartskante@Vorwärtskante}\index{Ruckwartskante@Rückwärtskante}
	Für eine Vorwärtskante \(e\) sei
	\(r\left(e\right) := c\left(e\right) - f\left(e\right)\),
	Für eine Rückwärtskante \(e\) sei 
	\(r\left(e\right) := f\left(e\right)\).

	Die \cdefname{Residualkapazität} von \(P\) ist definiert durch:
	\begin{align}
		r\left(P\right) &:=
			\min\left\{r\left(e_i\right) \mmid i \in \left\{1, \dotsc, l\right\}\right\}
	\end{align}
	Für \(l = 0\) sei \(r\left(P\right) := +\infty > 0\).
\end{definition}

Anhand der Residualkapazität kann dann der Fluss schrittweise erhöht werden,
indem der Fluss um die Residualkapaztiät von Vorwärtskanten erhöht wird.
Nach der Definition der Residualkapazität wird dadurch der Fluss erhöht,
ohne die Kapazität im Netzwerk zu überschreiten.
Dabei muss zusätzlich noch sichergestellt werden,
dass die \textsc{Kirchhoff}-Regel sichergestellt wird!
Dazu wird für jede Ecke sichergestellt,
dass die Flussbilanz an jeder Ecke stimmt,
d.\,h.~dass eine Veränderung des eingehenden Flusses auch
eine entsprechende Änderung des ausgehenden Flusses bewirkt.
Dabei werden drei Fälle unterschieden, die in \todo{bild 3.10} gezeigt sind:
\begin{enumerate}[(1)]
	\item Am Knoten sind zwei Vorwärtskanten.
		Dann wird auf beiden Seiten der Fluss erhöht,
		also erhöht sich der ausgehende und eingehende Fluss gleichermaßen.
	\item Am Knoten sind zwei Rückwärtskanten.
		Dann wird auf beiden Seiten der Fluss verringert,
		also verringert sich der ausgehende und eingehende Fluss gleichermaßen.
	\item Am Knoten ist eine Vorwärts- und eine Rückwärtskante.
		Je nach Richtung wird entweder der ausgehende oder der eingehende Fluss
		gleichermaßen erhöht und verringert,
		also ergibt sich keine Verändung der Bilanz.
		\todo{prüfen, ob Erklärung stimmt}
\end{enumerate}
Es lassen sich dann Schnitte finden,
deren ausgehende und eingehende Kanten Residualkapazität \(0\) haben.
Dessen Kapazität ist die Stärke des maximalen Flusses im Netzwerk.

\todo{bild 3.11}

\begin{beweis}
	Aus der o.\,g.~Vorüberlegung folgt bereits „\(\leq\)“.
	Die Richtung „\(\geq\)“ benötigt noch weitere Überlegungen.

	Sei \(N = \left(D, c, s, t\right)\) das fragliche Netzwerk.
	Sei \(f\) irgendein Fluss.

	Ist \(P\) ein Residualweg von \(s\) nach \(t\),
	so wird durch
	\begin{align}
		\left(f + P\right)\left(e\right) &:= \begin{cases}
			f\left(e\right) + r\left(P\right) & \left(\leq c\left(e\right)\right) \text{ für Vorwärtskante } e\\
			f\left(e\right) - r\left(P\right) & \left(\geq 0\right) \text{ für Rückwärtskante } e\\
			f\left(e\right) & \text{sonst}
		\end{cases}
	\end{align}
	ein Fluss \(f + P: E\left(D\right) \to \mathbb{R}_{\geq 0}\) definiert.

	\begin{align}
		\flowstrength{f + P} &= \flowstrength{f} + r\left(P\right)
	\end{align}
	Sei jetzt \(f\) ein Fluss maximaler Stärke.
	Dann gibt es keinen Residualweg von \(s\) nach \(t\)
	mit positiver Residualkapazität.
	Sei
	\begin{align}
		X &:= \left\{z \in V \mmid \exists \text{ Residualweg von } s \text{ nach } z
			\text{ mit positiver Residualkapazität.}\right\}
	\end{align}
	Wegen \(s \in X\) und \(t \notin X\) ist \(X\) ein Schnitt.

	Für \(e \in E_D^+\left(X\right)\) ist \(c\left(e\right) - f\left(e\right) = 0\),
	weil sonst auch die Ecken \(z \in V_D\left(e\right) \setminus X\)
	durch einen Residualweg positiver Residualkapazität erreichbar wäre.
	\todo{bild 3.11}
	Dann wären diese Ecken jedoch in \(X\) enthalten.\contrad

	Aus dem gleichen Grund ist für \(e \in E_D^-\left(X\right)\)
	gleich \(f\left(e\right) = 0\).
	Daher gilt
	\begin{align}
		\cutcap{X} &\overset{\mathclap{\text{Def.}}}{=} c\left(E_D^+\left(X\right)\right)\\
		&= f\left(E_D^+\left(X\right)\right) - 0\\
		&= f\left(E_D^+\left(X\right)\right) - f\left(E_D^-\left(X\right)\right)\\
		&\overset{\mathclap{\text{s.\,o.}}}{=} \flowstrength{f}\\
		\max\left\{\flowstrength{g} \mmid g \text{ ist Fluss}\right\}
		&\overset{\mathclap{\text{Wahl } f}}{=} \flowstrength{f}\\
		&= \cutcap{X}\\
		&\geq \min \left\{\cutcap{Y} \mmid Y \text{ Schnitt}\right\}
	\end{align}
\end{beweis}

Dieser Beweis lässt sich in einen einfachen Algorithmus
(genauer: die \textsc{Ford-Fulkerson}-Methode) überführen.
Dabei wir vom Nullfluss beginnend für alle Ecken berechnet,
ob Residualwege positiver Residualkapazität existieren.
Wenn man damit einen Residualweg von \(s\) nach \(t\) findet,
wird der Fluss entsprechend erhöht und iteriert.
Ansonsten endet der Algorithmus.

Bei diesem einfachen Algorithmus gibt es zwei grundsätzliche Probleme.
Werden die Residualkapazitäten in jedem Schritt kleiner,
besteht die Gefahr, dass der Algorithmus nicht terminiert.
Weiterhin ist nicht ganz klar, wie lange der Algorithmus läuft.
In der Vorlesung „Effiziente Algorithmen“
wird hier näher drauf eingegangen.
Es gibt auch schnellere Verfahren als dieser einfache Algorithmus.

Für ganzzahlige Flussprobleme sind einige Fragen leichter zu beantworten.
Beispielsweise ist dann klar, dass es bei immer kleiner werdenden Residualkapazitäten
irgendwann keinen Residualweg mit positiver Residualkapazität von \(s\) nach \(t\) gibt
und der Algorithmus terminieren muss.

\begin{satz}[Integrality Theorem)]\label{satz:integrality}
	Sei \(N\) ein Netzwerk mit ganzzahliger Kapazitätsfunktion.
	Dann gibt es einen ganzzahligen Fluss maximaler Stärke,
	der die Summe von ganzzahligen Elementarflüssen ist.
\end{satz}

Es ist bereits intuitiv klar,
dass ein Fluss maximaler Stärke auch ganzzahliger Stärke ist.
Es ist jedoch nicht sofort klar,
dass auch die Elementarflüsse maximaler Flüsse ganzzahliger Stärke sind,
da es auch prinzipiell möglich wäre,
reelle Stärken für Elementarflüsse zu wählen.

Wenn man jedoch mit einem Nullfluss beginnend einen maximalen Fluss
nach o.\,g. Algorithmus aufbaut,
sind in jedem Iterationsschitt alle Residualkapazitäten ganzzahlig,
da sie zu Beginn ganzzahlig sind und enstprechend dem Algorithmus
dann auch der Fluss nur um ganzzahlige Werte verändert wird.
Das wiederum bedeutet,
dass die neuen Residualkapazitäten sich auch nur ganzzahlig ändern
und damit auch ganzzahlig bleiben.

\begin{beweis}
	Wir beginnen mit dem Fluss \(f = 0\).
	Solange es einen Residualweg \(P\) von \(s\) nach \(t\)
	von positiver Residualkapazität \(r\left(P\right)\) gibt,
	setze \(f := f + P\), iteriere.
	Das Verfahren endet mit einem ganzzahligen Fluss \(f\)
	(da nur ganzzahlige Restkapazitäten in jedem Schritt hinzugefügt werden)
	und wie im vorangegangenen Beweis findet man einen Schnitt \(X\)
	mit \(\cutcap{X} = \flowstrength{f}\),
	d\,h.~\(f\) ist ein Fluss maximaler Stärke.

	Induktiv über die Stärke des ganzzahligen Flusses \(f\):

	Für \(\flowstrength{f} = 0\) sind alle Elementarflüsse trivialerweise ganzzahlig.\checkm
	
	Für \(\flowstrength{f} > 0\) existiert ein kontinuierlich gerichteter \(s,t\)-Weg \(P\)
	mit \(f\left(e\right) > 0\) für alle \(e \in E\left(P\right)\).
	Sonst ist die Menge
	\begin{align}
		X := \left\{z \in V\left(D\right) \mmid \exists s,z \text{-Weg} Q
		\text{ mit } f\left(e\right) > 0 \forall e \in Q\right\}
	\end{align}
	ein Schnitt und
	\begin{align}
		\flowstrength{f} &= \underbrace{f\left(E_D^+\left(X\right)\right))}_{0}
			- f\left(E_D^-\left(X\right)\right)\\
			&\leq 0\contrad
	\end{align}
	Definiere
	\begin{align}
		f'\left(e\right) &:= \begin{cases}
			f\left(e\right) - 1 & e \in E\left(P\right)\\
			f\left(e\right) & e \notin E\left(P\right)
		\end{cases}\\
		\shortintertext{und}
		c'\left(e\right) &:= \begin{cases}
			c\left(e\right) - 1 & e \in E\left(P\right)\\
			c\left(e\right) & e \notin E\left(P\right)
		\end{cases}
	\end{align}
	Dann ist \(f'\) ein Fluss im Netzwerk \(N' = \left(D, e', s, t\right)\)
	und ganzzahlig.
	Nach der Induktionsvoraussetzung gibt es einen Fluss \(g'\) in \(N'\)
	mit \(\flowstrength{g'} = \flowstrength{f'}\)
	und \(g'\) ist die Summe ganzzahliger Elementarflüsse.
	Dann ist \(g' + f_P\) (mit \(\flowstrength{f_P} = 1\) ebenfalls eine solche Summe
	und
	\begin{align}
		\flowstrength{g' + f_P} &= \flowstrength{g'} + \flowstrength{f_P}\\
		&= \flowstrength{f'} + 1\\
		&= \flowstrength{f}
	\end{align}
\end{beweis}

\section{Der Satz von Menger}\label{sec:satz-menger}

%\begin{satz}[\textsc{Menger}]\index{Menger}\label{satz:menger}
%\end{satz}

\begin{definition}[trennend!Kantenmenge]{trennende Kantenmenge}
	Seien \(s,t\) Ecken des Digraphen oder Graphen \(D\).
	Eine Menge \(F \subseteq E\left(D\right)\) heißt
	\cdefname{von} \(s\) \cdefname{nach} \(t\) \defname{}
	(für Graphen \(s,t\)-trennende Kantenmenge),
	falls \(D-F\) keinen \(s,t\)-Weg besitzt.
\end{definition}

Genau dann existiert ein solches \(F\),
wenn \(s \neq t\) ist.
Dies gilt offensichtlich, da \(F = E\left(D\right)\) eine solche trennende Kantenmenge ist
(ein Graph ohne Kanten hat schließlich auch niemals einen \(s,t\)-Weg).

\begin{definition}{kantendisjunkt}
	Zwei Wege von \(P, Q\) von \(D\) heißen \defname{},
	falls \(E\left(P\right) \cap E\left(Q\right) = \emptyset\) gilt.
\end{definition}

Im Folgenden wird auszugsweise eine Version der Sätze von Menger gezeigt.
Dazu gibt es eine ganze Reihe von Sätzen, die ähnlich sind.

\begin{satz}[\textsc{Menger}, lokale gerichtete Version]\label{satz:menger1}
	Seien \(s \neq t\) Ecken des Digraphen \(D\).
	Die maximale Anzahl paarweise kantendisjunkter \(s,t\)-Wege in \(D\)
	ist gleich der minimalen Anzahl von Kanten einer
	von \(s\) nach \(t\) trennenden Kantenmenge in \(D\).
\end{satz}

Dieses Theorem ist auch wieder von der Art Min-Max.

\begin{beweis}
	Sei \(k\) die maximale Anzahl paarweise kantendisjunkter \(s,t\)-Wege,
	\(l\) die minimale Anzahl von Kanten in einer von \(s\) nach \(t\) trennenden Kantenmenge.
	Jeder \(s,t\)-Weg enthält wenigstens eine Kante jeder
	von \(s\) nach \(t\) trennenden Kantenmenge.
	Dann \(k \leq l\).

	Betrachte das durch \(c : E\left(D\right) \to \mathbb{R}_{\geq 0}\),
	\(c\left(e\right) = 1 \forall e \in E\left(D\right)\) bestimmte Netzwerk
	\(N = \left(D, c, s, t\right)\).
	Sei \(X\) ein beliebiger Schnitt.
	Dann \(\cutcap{X} = \cabs{E_D^+\left(X\right)}\).
	Weil \(E_D^+\left(X\right)\) von \(s\) nach \(t\) trennende Kantenmenge ist,
	folgt \(\cutcap{X} = l\).
	Nach \autoref{satz:integrality} gibt es einen ganzzahligen Fluss der Stärke \(\geq l\),
	der die Summe von ganzzahligen Elementarflüssen \(f_{P_1}, \dotsc, f_{P_j}\) ist,
	wobei \(\flowstrength{f_{P_i}} \geq 1\),
	also \(\flowstrength{f_{P_i}} = 1\) gilt.
	Wegen \(\left(f_{P_1} + \dotsc + f_{P_j}\right)\left(e\right) \leq c\left(e\right) = 1\)
	sind die Wege \(P_1, \dotsc, P_j\) paarweise kantendisjunkt.
	Also \(j \geq l\).
	Also \(k \geq j\), damit auch \(k \geq l\).
\end{beweis}

\begin{definition}[trennend!Eckenmenge]{trennende Eckenmenge}
	Seien \(s, t\) Ecken des Digraphen oder Graphen \(G\).
	\(X \subseteq V\left(D\right) \setminus \left\{s, t\right\}\)
	heißt eine von \(s\) nach \(t\) \defname{}
	(im ungerichteten Fall \(s, t\)-\defname{}),
	wenn \(D - X\) keinen \(s, t\)-Weg besitzt.
\end{definition}

\todo{bild 3.13}

\begin{definition}{offen disjunkt}
	Zwei \(s, t\)-Wege heißen \defname{},
	falls sie keine inneren Ecken gemeinsam haben.
\end{definition}

Eine solche trennende Eckenmenge kann es nur geben, wenn \(s \neq t\)
und keine Kante \(\left(s, t\right) \in E\left(D\right)\) existiert,
da in dem Fall keine inneren Ecken existieren,
die man löschen könnte,
den Weg von \(s\) nach \(t\) zu zerstören.
Im entgegengesetzten Fall (d.\,h. es gibt eine innere Ecken auf dem \(s,t\)-Weg),
kann man immer eine trennende Eckenmenge
\(X = V\left(D\right) \setminus \left\{s, t\right\}\) wählen.

Genau dann existiert eine von \(s\) nach \(t\) trennende Eckenmenge,
wenn \(s \neq t\) und \(st \notin E\left(D\right)\).

\begin{folgerung}[\textsc{Menger}, lokale gerichtete Eckenversion]\label{satz:menger2}
	Seien \(s \neq t\) Ecken des Digraphen \(D\), \(st \notin E\left(D\right)\).
	Die maximale Anzahl paarweise offen disjunkter \(s,t\)-Wege
	ist gleich der mininmalen Anzahl von Ecken einer
	von \(s\) nach \(t\) trennenden Eckenmenge.
\end{folgerung}

Diese Folgerung lässt sich auf \autoref{satz:menger1} konstruktiv zurückführen.
Aus dem Graphen wird dabei ein Hilfsgraph gebildet,
in dem zwei besondere Ecken gefunden werden,
die genau dann durch eine bestimmte Anzahl kantendisjunkter Wege verbunden sind,
wenn die entsprechende Anzahl offendisjunkter Wege im ursprünglichen Graphen
vorhanden ist.
Dieser Hilfsgraph wird konstruiert,
indem für jede ursprüngliche Ecke \(x\) zwei neue Ecken \(x^-\) und \(x^+\) eingeführt werden,
und alle im ursprünglichen Graphen eingehenden Kanten an \(x^-\) eingehen
und alle im ursprünglichen Graphen ausgehenden Kanten von \(x^+\) ausgehen.
Zusätzlich wird jeweils eine Kante von \(x^-\) nach \(x^+\) eingefügt.
Es wird dann gezeigt, dass offendisjunkte Wege im ursprünglichen Graphen
korrespondierende kantendisjunkte Wege im Hilfsgraphen haben
und trennende Eckenmengen im ursprünglichen Graphen trennende Kantenmengen
im Hilfsgraphen haben,
sodass \autoref{satz:menger1} angewendet werden kann.

\begin{beweis}
	Sei \(D'\) ein Digraph mit den Ecken
	\(\left\{x^+, x^- \mmid x \in V\left(D\right)\right\}\)
	und allen Kanten aus \(M = \left\{x^-x^+ \mmid x \in V\left(D\right)\right\}\)
	sowie einer Kante \(y^+z^+\) für jedes \(yz \in E\left(D\right)\).

	\todo{bild 3.14}

	Ist \(P\) ein \(s,t\)-Weg in \(D\),
	etwa
	\begin{align}
		P &= \underbrace{x_0}_{s}, x_1, \dotsc, \underbrace{x_l}_{t}\text{.}\\
		\shortintertext{
			Dann ist \(P' := x_0^+, x_1^-, x_1^+, x_2^-, \dotsc, x_l^-\)
			ein \(s^+,t^-\)-Weg in \(D'\).
			Ist \(Q\) ein weiterer \(s,t\)-Weg in \(D\)
			und offendisjunkt zu \(P\),
			so ist \(Q'\) kantendisjunkt zu \(P'\) in \(D'\).
			Ist umgekehrt \(P'\) ein \(s^+,t^-\)-Weg in \(D'\),
			so hat \(P'\) die Form
		}
		P' &= \underbrace{x_0^+}_{s^+}, x_1^-, x_1^+, x_2^- \dotsc, \underbrace{x_l^-}_{t^-}
	\end{align}
	und \(P'' = x_0, \dotsc, x_l\) ist ein \(s,t\)-Weg in \(D\).
	Ist \(Q'\) ein weiterer \(s^+, t^-\)-Weg in \(D'\) und kantendisjunkt zu \(P'\),
	so ist \(Q''\) offendisjunkt zu \(P''\) in \(D\).

	Die maximale Anzahl \(l\) paarweise offendisjunkter \(s,t\)-Wege in \(D\) ist also gleich
	der Maximalzahl paarweise kantendisjunkter \(s^+,t^-\)-Wege in \(D'\).

	Sei \(S\) eine von \(s\) nach \(t\) trennende Eckenmenge in \(D\)
	von kleinster Mächtigkeit \(\cabs{S} = l\).
	Dann \(S' = \left\{x^-x^+ \mmid x \in S\right\}\) ist eine
	von \(s^+\) nach \(t^-\) trennende Kantenmenge in \(D\).
	Sonst gäbe es ja ein \(s^+, t^-\)-Weg in \(D'\) ohne Kanten aus \(S'\), etwa \(P'\),
	und \(P''\) (wie oben) wäre ein \(s,t\)-Weg in \(D\) ohne Ecken aus \(S\).\contrad

	\(S'\) ist sogar eine von \(s^+\) nach \(t^-\) trennende Kantenmenge in \(D'\)
	von kleinster Mächtigkeit:
	Sei dazu \(T'\) irgendeine von \(s^+\) nach \(t^-\) trennende Kantenmenge in \(D'\).
	Für \(e \in M\) sei \(\overline{e} := e\).
	Für \(e = y^+z^-\) setze \(\overline{e} := z^-z^+\) für \(z \neq t\),
	für \(e = y^+t^-\) setze \(\overline{e} = y^-y^+\).

	\todo{bild 3.15}

	Jeder \(s^+,t^-\)-Weg, der ein \(e\) aus \(T'\) enthält,
	enthält auch \(\overline{e}\).
	Kein solcher Weg enthält die Kanten \(s^-s^+\) oder \(t^-t^+\).
	\(\overline{T'} := \left\{\overline{e} \mmid e \in T'\right\} \setminus \left\{s^-s^+, t^-t^+\right\}\)
	ist dann eine von \(s^+\) nach \(t^-\) trennende Kantenmenge in \(D'\) mit
	\(\cabs{\overline{T'}} \leq \cabs{T'}\).
	Dann ist \(T'' := \left\{x \mmid x^-x^+ \in \overline{T'}\right\}\) eine
	von \(S\) nach \(T\) trennende Eckenmenge in \(D\).
	Sonst gäbe es einen \(s,t\)-Weg in \(D\) ohne Ecken aus \(T''\), etwa \(P\)
	und \(P'\) (wie oben) wäre \(s^+,t^-\)-Weg in \(D'\) ohne Kanten in \(\overline{T'}\).\contrad

	Also ist
	\begin{align}
		\cabs{S'} = \cabs{S} \leq \cabs{T''} = \cabs{\overline{T'}} \leq \cabs{T'}\text{.}
	\end{align}

	Also ist die Anzahl \(k\) von Ecken einer kleinsten
	von \(s\) nach \(t\) trennenden Eckenmenge in \(D\) gleich der Anzahl \(k'\)
	von Kanten einer von \(s^+\) nach \(t^-\) trennenden Kantenmenge in \(D'\).
	Nach \autoref{satz:menger1} für \(D'\), \(s^+\), \(t^-\) gilt \(k' = l'\).
	Da \(k' = k\) und \(l' = l\), ist \(k = l\).
\end{beweis}

\begin{folgerung}[\textsc{Menger}, lokale ungerichtete Eckenversion]\label{satz:menger3}
	Seien \(s \neq t\) nicht benachbarte Ecken des Graphen \(G\).
	Die maximale Anzahl paarweise offendisjunkter \(s,t\)-Wege in \(G\)
	ist gleich der Minimalanzahl von Ecken einer \(s,t\)-trennenden Eckenmenge.
\end{folgerung}

Auch diese Satz lässt sich leicht auf die gerichtete Version zurückführen.
Dafür wird aus dem ungerichteten Graphen ein Digraph konstruiert,
bei dem für jede ungerichtete Kante im ungerichteten Graphen
zwei gerichtete Kanten (eine pro Richtung) eingefügt werden \todo{bild 3.16}.

\begin{beweis}
	Sei \(D\) der Digraph mit \(V\left(D\right) = V\left(G\right)\)
	und Kanten \(xy\) für in \(G\) benachbarte Ecken \(x, y\).
	Jeder \(s,t\)-Weg in \(G\) (in Folgendarstellung) ist ein \(s,t\)-Weg in \(D\)
	und umgekehrt.
	Mit \autoref{satz:menger2} folgt die Behauptung.
\end{beweis}

Diese Sätze werden nun ausgeweitet auf globale Versionen, d.\,h.
es werden alle Eckenpaare im Graphen betrachtet.

\begin{definition}[zusammenhangend@zusammenhängend!k@\(k\)-]{zusammenhängend}
	Ein Graph \(G\) heißt \(k\)-\defname{},
	falls \(\cabs{V\left(G\right)} > k\) gilt
	und \(G - S\) zusammenhängend ist für alle \(S \subseteq V\left(G\right)\)
	mit \(\cabs{S} < k\).
\end{definition}

Der Graph ist also zusammenhängend, solange weniger als \(k\) Ecken gelöscht werden.
Dies ist auch in Netzen (bspw. im Stromnetz) ein Maß für die Ausfallsicherheit,
da \(k\)-zusammenhängende Netze mit großem \(k\) viele Knoten ausfallen müssen,
damit es zu einem Netzausfall kommt.

\begin{definition}[trennend!Eckenmenge]{trennende Eckenmenge}
	Ein \(T \subseteq V\left(G\right)\) heißt \defname{} von \(G\),
	falls \(G - T\) unzusammenhängend ist.
\end{definition}

\begin{folgerung}[\textsc{Menger}, globale ungerichtete Eckenversion]\label{satz:menger4}
	Ein Graph \(G\) mit \(\cabs{V\left(G\right)} > 1\) ist genau dann \(k\)-zusammenhängend,
	wenn es zwischen je zwei Ecken \(k\) offen disjunkte Wege gibt.
\end{folgerung}

\begin{beweis}
	\begin{itemize}
		\item[„\(\to\)“]
			Sei \(G\) \(k\)-zusammenhängend, \(s \neq t\) seien Ecken in \(G\).

			Im Fall \(st \notin E\left(G\right)\) folgt die Existenz von \(k\)
			offendisjunkten \(s,t\)-Wegen direkt aus \autoref{satz:menger3},
			weil es nach Voraussetzung insbesondere keine \(s,t\)-trennende Eckenmenge
			mit weniger als \(k\) Ecken gibt.

			Fall \(st \in E\left(G\right)\):
			Betrachte \(G - st\).
			Gäbe es eine \(s,t\)-trennende Eckenmenge \(T\) von \(G - st\) mit \(\cabs{T} < k-1\),
			so sei \(C\) die Komponente von \(\left(G - st\right) - T\),
			welche \(s\) enthält und \(C'\) die Vereinigung aller anderen Komponenten
			von \(\left(G - st\right) - T\).

			Im Fall \(\cabs{V\left(C\right)} > 1\) ist \(T \cup \left\{s\right\}\)
			eine trennende Eckenmenge von \(G - st\) und auch von \(G\).\contrad[\cabs{T \cup \left\{s\right\}} < k]

			Sonst ist \(\cabs{V\left(C'\right)} > 1\).
			Wegen \(\cabs{V\left(G\right)} > k\) und \(T \cup \left\{t\right\}\)
			trennende Eckenmenge von \(G\) mit weniger als \(k\) Elementen.

			Aufgrund von \autoref{satz:menger3} besitzt \(G - st\) \(k-1\) viele
			offen disjunkte \(s,t\)-Wege;
			zusammen mit dem Weg \(P = st\) erhält man \(k\) viele offen disjunkte \(s,t\)-Wege
			in \(G\).\checkm

		\item[„\(\gets\)“]
			Es gibt \(s \neq t\) aus \(V\left(G\right)\);
			nach Voraussetzung existieren \(k\) offen disjunkte \(s,t\)-Wege.\footnote{
				Der Fall, dass einer der Wege keine inneren Ecken hat,
				kann auch eintreten.
			}
			Wenigstens \(k-1\) dieser Wege enthalten eine innere Ecke.
			Also \(\cabs{V\left(G\right)} > k\).

			Angenommen \(S \subseteq V\left(G\right)\), \(\cabs{S} < k\) und
			\(G - S\) unzusammenhängend,
			so gibt es \(s,t\) aus verschiedenen Komponenten von \(G - S\)
			und \(S\) ist eine \(s,t\)-trennende Eckenmenge in \(G\)\footnote{
				Dies kann es aufgrund der lokalen Version des Satzes nicht geben.
			}.
			Also gibt es keine \(k\) vielen offen disjunkte \(s,t\)-Wege in \(G\).\footnote{
				Wenn es diese Wege gäbe, müsste es von jedem Weg eine innere Ecke
				im \(s,t\)-Trenner geben.
				Dann müsste \(S\) aber größer sein.
			}\contrad
	\end{itemize}
\end{beweis}

\section{Der Satz von \textsc{Gutnikov}}

Zu beachten ist, dass die lokalen Versionen des Satzes von \textsc{Menger}
immer zusätzliche Voraussetzungen haben.
Was haben jedoch diese Voraussetzungen gemeinsam
und wie weit lässt sich der Satz von \textsc{Menger} verallgemeinern?
Dabei stellt sich heraus, dass bereits ein entsprechender verallgemeinerter Satz
in Russland publiziert wurde.
Dies ist der Satz von \textsc{Gutnikov}
und verallgemeinert die Sätze von \textsc{Menger} auf allgemeine Graphen.

\begin{definition}[Graph!allgemein]{allgemeiner Graph}
	Ein \defname{} \(G = \left(V, E, \mathrm{init}, \mathrm{ter}\right)\)
	ist ein Quadrupel disjunkter Mengen \(V\), \(E\) und Abbildungen
	\(\mathrm{init}, \mathrm{ter}: E\left(G\right) \to \powset\left(V\left(G\right)\right)\).
\end{definition}

Die Funktionen \(\mathrm{init}\) und \(\mathrm{ter}\) ordnen jeder Kante
eine Start- und Endecke zu.
Die Funktion \(\mathrm{init}\) zeigt dabei die Start- und
\(\mathrm{ter}\) die Endecke an.
So können sowohl ungerichtete als auch gerichtete Graphen
gleichermaßen betrachtet werden.
Die Menge \(V\) und \(G\) sind weiterhin Ecken und Kanten,
wie sie bereits von den bisher behandelten Graphen bekannt sind.

Im Gegensatz zu den bisherigen Graphen können Kanten jedoch
Mengen von Ecken als Start- und Mengen von Ecken als Endpunkte haben.

\todo{schöne Abbildung}

\begin{definition}[Weg!allgemeiner Graph]{Weg}
	Eine Folge \(P = x_0, e_1, x_1, e_2, \dotsc, e_l, x_l\)
	heißt \(a,b\)-\defname{} in \(G\),
	wenn \(x_0 = a\), \(x_l = b\),
	\(x_0, \dotsc, x_l\) paarweise verschiedene Elemente aus \(V\) und
	\(e_1, \dotsc, e_l\) paarweise verschiedene Elemente aus \(E\) sind und
	\(x_{i-1} \in \mathrm{init}\left(e_i\right)\), \(x_i \in \mathrm{init}\left(e_i\right)\) \(\forall i \in \left\{1, \dotsc, l\right\}\).
	Für \(A, B \subseteq V\) ist ein \(A,B\)-\defname{} ein \(a,b\)-Weg
	mit \(a \in A\), \(b \in B\).
\end{definition}

\begin{definition}[Separator!allgemeiner Graph]{Separator}
	\(S \subseteq V \cup E\) heißt \(A, B\)-\defname{},
	falls jeder \(A,B\)-Weg ein Element aus \(S\) besitzt.
	Zwei Wege heißen \(S\)-\cdefname{disjunkt}\index{disjunkt!\(S\)},
	falls sie keine gemeinsamen Elemente aus \(S\) besitzen.
\end{definition}

Im Satz von \textsc{Gutnikov} fallen nun
alle Voraussetzungen im zusammen,
die es zuvor in den lokalen Versionen der Sätze von \textsc{Menger} gab.

\begin{satz}[\textsc{Gutnikov}]\index{Gutnikov}
	Sei \(G\) ein allgemeiner Graph,
	\(S \subseteq V\left(G\right) \cup E\left(G\right)\)
	ein \(A,B\)-Separator für \(A, B \subseteq V\left(G\right)\).
	Genau dann existieren \(k\) viele paarweise \(S\)-disjunkte \(A,B\)-Wege,
	wenn es keinen \(A,B\)-Separator \(T \subseteq S\) mit \(\cabs{T} < k\) gibt.

	(Ohne Beweis)
\end{satz}

Durch Spezialisierung lassen sich aus diesem Satz dann auch wieder
die Sätze von \textsc{Menger} aus \autoref{sec:satz-menger} zurückführen.
