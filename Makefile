EXTRA_SRCS = $(wildcard img/*.tex) $(wildcard chap*.tex) appendix.tex
LTX = latexmk -xelatex -file-line-error -interaction=nonstopmode -halt-on-error

all: vl.pdf

%.pdf: %.tex $(EXTRA_SRCS) vl.sty
	$(LTX) $<
